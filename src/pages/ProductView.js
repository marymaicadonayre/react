

import {useState, useEffect} from 'react';
import{Container, Card, Button, Row, Col} from 'react-bootstrap';
//kukunin ang params na id in order for us to have access to its object
import{useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductView() {

  const [productName, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const history = useNavigate();
  const{productId} = useParams();
  

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_URL}/products/getSpecificProduct/${productId}`)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          /*const { name, description price } = data;*/
          
          setName(data.productName);
          setDescription(data.description);
          setPrice(data.price);
        })



      }, [productId])


const order = (productId) => {
  fetch(`${process.env.REACT_APP_URL}/orders/addToCart/${productId}`,{
    method: "POST",
    headers: {
      "Content-Type" : "application/json",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => response.json())
  .then(data => {
    if(data === true){
      Swal.fire({
        title: "Added To Cart Successfully!!",
        icon: "success",
      })
      history("/products");
    }else{
      Swal.fire({
        title: "Please Log In!",
        icon: "error",
        // text: "Try Again!"
      })
      history("/");
    }
  })
}


  return(
    <Container className="mt-5">
      <Row>
        <Col lg = {{span:6, offset: 3}}>
          <Card>
            <Card.Body className="text-center mt-5">
              <Card.Title>{productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Button variant="primary" onClick = {() => order(productId)}>Add To Cart</Button>
            </Card.Body>  
          </Card>         
        </Col>
      </Row>
    </Container>

    )
}